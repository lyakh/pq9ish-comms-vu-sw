/*
 *  PQ9ISH COMMS: comms module, part of an open source pocketqube stack
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INC_MANIFESTO_H_
#define INC_MANIFESTO_H_

static const char manifesto[429] = {'L', 'I', 'B', 'R', 'E', ' ', 'S', 'P', 'A', 'C', 'E', ' ',
                                    'M', 'A', 'N', 'I', 'F', 'E', 'S', 'T', 'O', '.', 'A', 'l', 'l', ' ', 'p', 'e',
                                    'o', 'p', 'l', 'e', ' ', 's', 'h', 'a', 'l', 'l', ' ', 'h', 'a', 'v', 'e', ' ',
                                    't', 'h', 'e', ' ', 'r', 'i', 'g', 'h', 't', ' ', 't', 'o', ' ', 'e', 'x', 'p',
                                    'l', 'o', 'r', 'e', ' ', 'a', 'n', 'd', ' ', 'u', 's', 'e', ' ', 'o', 'u', 't',
                                    'e', 'r', ' ', 's', 'p', 'a', 'c', 'e', ' ', 'f', 'o', 'r', ' ', 't', 'h', 'e',
                                    ' ', 'b', 'e', 'n', 'e', 'f', 'i', 't', ' ', 'a', 'n', 'd', ' ', 'i', 'n', ' ',
                                    't', 'h', 'e', ' ', 'i', 'n', 't', 'e', 'r', 'e', 's', 't', 's', ' ', 'o', 'f',
                                    ' ', 'a', 'l', 'l', ' ', 'h', 'u', 'm', 'a', 'n', 'i', 't', 'y', '.', ' ', 'E',
                                    'x', 'p', 'l', 'o', 'r', 'a', 't', 'i', 'o', 'n', ' ', 'a', 'n', 'd', ' ', 'u',
                                    's', 'e', ' ', 'o', 'f', ' ', 'o', 'u', 't', 'e', 'r', ' ', 's', 'p', 'a', 'c',
                                    'e', ' ', 's', 'h', 'a', 'l', 'l', ' ', 'b', 'e', ' ', 'c', 'a', 'r', 'r', 'i',
                                    'e', 'd', ' ', 'o', 'u', 't', 'c', 'o', 'l', 'l', 'a', 'b', 'o', 'r', 'a', 't',
                                    'i', 'v', 'e', 'l', 'y', ' ', 'a', 'n', 'd', ' ', 'c', 'o', 'o', 'p', 'e', 'r',
                                    'a', 't', 'i', 'v', 'e', 'l', 'y', '.', 'O', 'u', 't', 'e', 'r', ' ', 's', 'p',
                                    'a', 'c', 'e', ' ', 's', 'h', 'a', 'l', 'l', ' ', 'b', 'e', ' ', 'u', 's', 'e',
                                    'd', ' ', 'e', 'x', 'c', 'l', 'u', 's', 'i', 'v', 'e', 'l', 'y', ' ', 'f', 'o',
                                    'r', ' ', 'p', 'e', 'a', 'c', 'e', 'f', 'u', 'l', ' ', 'p', 'u', 'r', 'p', 'o',
                                    's', 'e', 's', '.', ' ', 'P', 'r', 'o', 'f', 'i', 't', ' ', 's', 'h', 'a', 'l',
                                    'l', ' ', 'n', 'o', 't', ' ', 'b', 'e', ' ', 't', 'h', 'e', ' ', 'd', 'r', 'i',
                                    'v', 'i', 'n', 'g', ' ', 'f', 'o', 'r', 'c', 'e', ' ', 'f', 'o', 'r', ' ', 's',
                                    'p', 'a', 'c', 'e', ' ', 'e', 'x', 'p', 'l', 'o', 'r', 'a', 't', 'i', 'o', 'n',
                                    '.', ' ', 'A', 'l', 'l', ' ', 'p', 'e', 'o', 'p', 'l', 'e', ' ', 's', 'h', 'a',
                                    'l', 'l', ' ', 'h', 'a', 'v', 'e', ' ', 'a', 'c', 'c', 'e', 's', 's', ' ', 't',
                                    'o', ' ', 'o', 'u', 't', 'e', 'r', ' ', 's', 'p', 'a', 'c', 'e', ',', ' ', 's',
                                    'p', 'a', 'c', 'e', ' ', 't', 'e', 'c', 'h', 'n', 'o', 'l', 'o', 'g', 'i', 'e',
                                    's', ' ', 'a', 'n', 'd', ' ', 's', 'p', 'a', 'c', 'e', ' ', 'd', 'a', 't', 'a', '.'
                                   };

#endif /* INC_MANIFESTO_H_ */
